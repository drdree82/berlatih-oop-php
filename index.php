<?php

//Release 0

require_once 'animal.php';

$sheep = new Animal ("shaun");
$sheep -> set_name('shaun');
$sheep -> set_legs('2');
$sheep -> set_cold_blooded('false');

echo $sheep -> get_name().'<br>'; // "shaun"
echo $sheep -> get_legs().'<br>'; // 2
echo $sheep -> get_cold_blooded().'<br>'; // false
echo '<br>';

//Ape
$sungokong = new Ape("kera sakti");
echo $sungokong->get_yell().'<br>'; // "Auooo"
echo '<br>';

//Frog
$kodok = new Frog("buduk");
echo $kodok->get_jump(); // "hop hop"


?>