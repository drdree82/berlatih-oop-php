<?php

//Release 0

class Animal{

	public $name;
	public $legs;
	public $cold_blooded;

	public function set_name($nama){
		$this->name = $nama;
	}

	public function set_legs($kaki){
		$this->legs = $kaki;
	}
	public function set_cold_blooded($darah){
		$this->cold_blooded = $darah;
	}

	public function get_name(){
		return $this->name;
	}
	public function get_legs(){
		return $this->legs;
	}
	public function get_cold_blooded(){
		return $this->cold_blooded;
	}
}

?>